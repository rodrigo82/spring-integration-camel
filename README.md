# Project Camel + Spring Integration

## Building the project
Since it's a simple Spring Boot application the building command is simple:  
`mvn clean package`

## Running
To run the application just use the following command:  
`java -jar -DapiKey=<your Google api key> target/geocode-1.0.0.jar`

## Testing
A very simple test is use Linux command curl to make a request to the api while it is running in your local machine:  
`curl -X GET "http://localhost:8080/address/coordinates?address=av.+da+liberdade+110"`

