package com.backbase.evaluation;

import com.backbase.evaluation.model.Address;
import com.backbase.evaluation.model.GeocodeResponse;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class CamelRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("direct:getGeo")
                .filter(body().isNotEqualTo(""))
                .setHeader(Exchange.HTTP_QUERY, simple("address=${in.body}&key={{geocode.key}}"))
                .to("https4:{{geocode.url}}")
                .unmarshal().jacksonxml(GeocodeResponse.class)
        ;
    }
}
