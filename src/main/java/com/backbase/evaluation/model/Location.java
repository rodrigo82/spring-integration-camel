package com.backbase.evaluation.model;

import lombok.Data;

@Data
public class Location {

    private Double lat;
    private Double lng;
}
