package com.backbase.evaluation.model;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Address {

    @JacksonXmlProperty(localName = "formatted_address")
    private String formattedAddress;
    private Geometry geometry;

}
