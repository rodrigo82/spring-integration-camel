package com.backbase.evaluation;

import org.apache.camel.*;
import org.apache.camel.impl.DefaultEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
@ImportResource("classpath:integration.xml")
public class Main {

    public static void main(String[] args) {
        run(Main.class, args);
    }

    @Autowired
    public void camelContext(ProducerTemplate producerTemplate) {
        producerTemplate.setDefaultEndpointUri("direct:getGeo");
    }

}
